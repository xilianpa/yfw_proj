define(["jquery"], function(){
	// 异步加载头部静态HTML资源
	$.ajax("/html/include/header.html").done(function(data){
		// 将HTML字符串添加到 <header> 标签内部
		$("header").html(data);
	}).done(function(){
		// 后继处理
		// ....
	}).done(function(){
		
	});
});